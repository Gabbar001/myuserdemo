package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.UserRequest;
import com.wecanfit.common.dto.myUser.response.UserFullResponse;
import com.wecanfit.common.dto.myUser.response.UserResponse;
import com.wecanfit.myuser.entity.UserEntity;
import org.modelmapper.ModelMapper;

public class UserMapper {

    public static UserEntity requestToEntity(UserRequest request) {
        return new ModelMapper().map(request, UserEntity.class);
    }

    public static void requestToEntity(UserRequest request, UserEntity entity) {
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
        entity.setAbout(request.getAbout());
    }

    public static UserFullResponse requestToFullResponse(UserEntity entity) {
        return new ModelMapper().map(entity, UserFullResponse.class);
    }

    public static UserResponse requestToResponse(UserEntity entity) {
        return new ModelMapper().map(entity, UserResponse.class);
    }
}
