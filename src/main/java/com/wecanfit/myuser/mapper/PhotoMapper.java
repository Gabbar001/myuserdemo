package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.PhotoRequest;
import com.wecanfit.common.dto.myUser.response.PhotoResponse;
import com.wecanfit.myuser.entity.PhotoEntity;
import org.modelmapper.ModelMapper;

public class PhotoMapper {

    public static PhotoEntity requestToEntity(PhotoRequest request) {
        return new ModelMapper().map(request, PhotoEntity.class);
    }

    public static PhotoResponse entityToResponse(PhotoEntity entity) {
        return new ModelMapper().map(entity, PhotoResponse.class);
    }

}
