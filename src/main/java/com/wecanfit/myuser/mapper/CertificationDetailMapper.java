package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.CertificationDetailRequest;
import com.wecanfit.common.dto.myUser.response.CertificationDetailResponse;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.CertificationDetailEntity;

public class CertificationDetailMapper {
    
    public static CertificationDetailEntity requestToEntity(CertificationDetailEntity entity, CertificationDetailRequest request) {
        if(Util.isNullOrEmpty(entity)){
            entity = new CertificationDetailEntity();
        }
        entity.setName(request.getName());
        entity.setCredentialId(request.getCredentialId());
        entity.setCredentialUrl(request.getCredentialUrl());
        entity.setExpired(request.getIsExpired());
        entity.setOrganisation(request.getOrganisation());
        entity.setDescription(request.getDescription());
        if(!Util.isNullOrEmpty(request.getIssueDate())){
            entity.setIssueDate(Util.timestampToDate(request.getIssueDate()));
        }
        if(!Util.isNullOrEmpty(request.getExpirationDate())){
            entity.setExpirationDate(Util.timestampToDate(request.getExpirationDate()));
        }
        return entity;
    }

    public static CertificationDetailResponse entityToResponse(CertificationDetailEntity entity){
        CertificationDetailResponse response = new CertificationDetailResponse();
        response.setName(entity.getName());
        response.setCredentialId(entity.getCredentialId());
        response.setCredentialUrl(entity.getCredentialUrl());
        response.setDescription(entity.getDescription());
        if(entity.getExpired()){
            response.setExpirationDate(entity.getExpirationDate().getTime());
        }
        response.setIssueDate(entity.getIssueDate().getTime());
        response.setOrganisation(entity.getOrganisation());
        response.setIsExpired(entity.getExpired());
        response.setId(entity.getId());
        return response;
    }

}
