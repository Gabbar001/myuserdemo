package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.SupportRequest;
import com.wecanfit.common.dto.myUser.response.SupportResponse;
import com.wecanfit.myuser.entity.SupportEntity;
import org.modelmapper.ModelMapper;

public class SupportMapper {

    public static SupportEntity requestToEntity(SupportRequest request) {
        return new ModelMapper().map(request, SupportEntity.class);
    }

    public static SupportResponse entityToResponse(SupportEntity entity) {
        return new ModelMapper().map(entity, SupportResponse.class);
    }

    public static void requestToEntity(SupportRequest request, SupportEntity entity) {
        new ModelMapper().map(request, entity);
    }
}
