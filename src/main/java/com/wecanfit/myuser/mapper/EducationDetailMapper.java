package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.EducationDetailRequest;
import com.wecanfit.common.dto.myUser.response.EducationDetailResponse;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.EducationDetailEntity;

public class EducationDetailMapper {

    public static EducationDetailEntity requestToEntity(EducationDetailEntity entity, EducationDetailRequest request) {
        if(Util.isNullOrEmpty(entity)){
            entity = new EducationDetailEntity();
        }
        entity.setActivitiesAndSocieties(request.getActivitiesAndSocieties());
        entity.setDegree(request.getDegree());
        entity.setFieldOfStudy(request.getFieldOfStudy());
        entity.setSchool(request.getSchool());
        entity.setDescription(request.getDescription());
        if(!Util.isNullOrEmpty(request.getStartDate())){
            entity.setStartDate(Util.timestampToDate(request.getStartDate()));
        }
        if(!Util.isNullOrEmpty(request.getEndDate())){
            entity.setEndDate(Util.timestampToDate(request.getEndDate()));
        }
        return entity;
    }

    public static EducationDetailResponse entityToResponse(EducationDetailEntity entity){
        EducationDetailResponse response = new EducationDetailResponse();
        response.setActivitiesAndSocieties(entity.getActivitiesAndSocieties());
        response.setDegree(entity.getDegree());
        response.setDescription(entity.getDescription());
        response.setEndDate(entity.getEndDate().getTime());
        response.setStartDate(entity.getStartDate().getTime());
        response.setFieldOfStudy(entity.getFieldOfStudy());
        response.setId(entity.getId());
        response.setSchool(entity.getSchool());
        return response;
    }

}
