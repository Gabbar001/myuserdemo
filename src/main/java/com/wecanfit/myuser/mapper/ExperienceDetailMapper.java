package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.ExperienceDetailRequest;
import com.wecanfit.common.dto.myUser.response.ExperienceDetailResponse;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.ExperienceDetailEntity;

public class ExperienceDetailMapper {

    public static ExperienceDetailEntity requestToEntity(ExperienceDetailEntity entity,ExperienceDetailRequest request) {
        if(Util.isNullOrEmpty(entity)){
            entity = new ExperienceDetailEntity();
        }
        entity.setCentreName(request.getCentreName());
        entity.setCurrentJob(request.getCurrentJob());
        entity.setDescription(request.getDescription());
        if(!Util.isNullOrEmpty(request.getStartDate())){
            entity.setStartDate(Util.timestampToDate(request.getStartDate()));
        }
        if(!Util.isNullOrEmpty(request.getEndDate())){
            entity.setEndDate(Util.timestampToDate(request.getEndDate()));
        }
        entity.setTitle(request.getTitle());
        entity.setLocation(request.getLocation());
        entity.setEmploymentType(request.getEmploymentType());
        return entity;
    }

    public static ExperienceDetailResponse entityToResponse(ExperienceDetailEntity entity) {
        ExperienceDetailResponse response = new ExperienceDetailResponse();
        response.setStartDate(entity.getStartDate().getTime());
        if(!entity.getCurrentJob()){
            response.setEndDate(entity.getEndDate().getTime());
        }
        response.setIsCurrentJob(entity.getCurrentJob());
        response.setCentreName(entity.getCentreName());
        response.setEmploymentType(entity.getEmploymentType());
        response.setDescription(entity.getDescription());
        response.setTitle(entity.getTitle());
        response.setLocation(entity.getLocation());
        response.setId(entity.getId());
        return response;
    }




}
