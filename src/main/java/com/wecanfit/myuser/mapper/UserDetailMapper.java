package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.UserDetailRequest;
import com.wecanfit.myuser.entity.UserDetailEntity;
import org.modelmapper.ModelMapper;

public class UserDetailMapper {

    public static UserDetailEntity requestToEntity(UserDetailRequest request) {
        return new ModelMapper().map(request, UserDetailEntity.class);
    }

    public static void requestToEntity(UserDetailRequest request, UserDetailEntity entity) {
        new ModelMapper().map(request, entity);
    }

}
