package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.response.LoginResponse;
import com.wecanfit.common.dto.myUser.response.UserResponse;
import com.wecanfit.myuser.entity.UserEntity;
import org.modelmapper.ModelMapper;

public class LoginResponseMapper {

    public static LoginResponse requestToEntity(Object token){
        LoginResponse response = new LoginResponse();
        response.setToken(token);
        return response;
    }
}
