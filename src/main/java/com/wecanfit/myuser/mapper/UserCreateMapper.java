package com.wecanfit.myuser.mapper;

import com.wecanfit.common.dto.myUser.request.UserCreateRequest;
import com.wecanfit.common.dto.myUser.request.UserDetailRequest;
import com.wecanfit.common.dto.myUser.request.UserRequest;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.UserDetailEntity;
import com.wecanfit.myuser.entity.UserEntity;
import org.modelmapper.ModelMapper;

import java.util.Random;

public class UserCreateMapper {

    public static UserEntity requestToEntity(UserCreateRequest request) {
        UserEntity userEntity = new UserEntity();
        userEntity.setPhoneNo(request.getPhoneNo());
        userEntity.setFirstName(request.getFirstName());
        userEntity.setLastName(request.getLastName());
        userEntity.setUsername(request.getUsername());
        return userEntity;
    }

    public static UserEntity requestToEntityWithManualUsername(UserCreateRequest request) {
        UserEntity userEntity = new UserEntity();
        userEntity.setPhoneNo(request.getPhoneNo());
        userEntity.setFirstName(request.getFirstName());
        userEntity.setLastName(request.getLastName());

        Random random = new Random();
        userEntity.setUsername(request.getFirstName()+String.format("%04d", random.nextInt(10000)));
        return userEntity;
    }

    public static UserDetailEntity requestToUserDetailEntity(UserDetailRequest request) {
        if(!Util.isNullOrEmpty(request)) {
            return new ModelMapper().map(request, UserDetailEntity.class);
        }else {
            UserDetailEntity userDetail = new UserDetailEntity();
            return userDetail;
        }
    }
}
