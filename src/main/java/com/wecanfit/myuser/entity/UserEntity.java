package com.wecanfit.myuser.entity;

import com.wecanfit.common.dto.myUser.enums.UserType;
import com.wecanfit.common.enums.Group;
import com.wecanfit.myuser.entity.audit.BaseEntity;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.security.SecureRandom;
import java.util.*;

@Entity
@Table(name = "user")
public class UserEntity extends BaseEntity implements UserDetails {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "user_name", unique = true)
    private String username;

    @Column(name = "about")
    private String about;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_no", unique = true)
    private String phoneNo;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private UserType type = UserType.USER;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "detail_id", referencedColumnName = "id")
    private UserDetailEntity detail;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stats_id", referencedColumnName = "id")
    private UserStats stats;

    @ManyToMany()
    @JoinTable(
            name = "user_group",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    private Set<GroupEntity> groups = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cover_photo_id")
    private PhotoEntity coverPhoto;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "profile_photo_id")
    private PhotoEntity profilePhoto;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "user",
            orphanRemoval = true)
    private Set<EducationDetailEntity> educationDetails = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "user",
            orphanRemoval = true)
    private Set<ExperienceDetailEntity> experienceDetails = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "user",
            orphanRemoval = true)
    private Set<CertificationDetailEntity> certificationDetails = new HashSet<>();

    private boolean enabled = true;
    private boolean accountNonLocked = true;
    private boolean accountNonExpired = true;
    private boolean credentialsNonExpired = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public UserStats getStats() {
        return stats;
    }

    public void setStats(UserStats stats) {
        this.stats = stats;
    }
    
    public Set<GroupEntity> getGroups() {
        return groups;
    }

    public void setGroups(Set<GroupEntity> groups) {
        this.groups = groups;
    }

    public void addGroup(GroupEntity group) {
        this.groups.add(group);
    }

    public void removeGroup(GroupEntity group){
        this.groups.remove(group);
    }

    public PhotoEntity getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(PhotoEntity coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public PhotoEntity getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(PhotoEntity profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for(GroupEntity group: this.groups){
            for(RoleEntity role: group.getRoles()){
                authorities.addAll(role.getPrivileges());
            }
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        int strength = 10; // work factor of bcrypt
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
        this.password = bCryptPasswordEncoder.encode(password);
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserDetailEntity getDetail() {
        return detail;
    }

    public void setDetail(UserDetailEntity detail) {
        this.detail = detail;
    }


    public Set<EducationDetailEntity> getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(Set<EducationDetailEntity> educationDetails) {
        this.educationDetails = educationDetails;
    }

    public Set<ExperienceDetailEntity> getExperienceDetails() {
        return experienceDetails;
    }

    public void setExperienceDetails(Set<ExperienceDetailEntity> experienceDetails) {
        this.experienceDetails = experienceDetails;
    }

    public Set<CertificationDetailEntity> getCertificationDetails() {
        return certificationDetails;
    }

    public void setCertificationDetails(Set<CertificationDetailEntity> certificationDetails) {
        this.certificationDetails = certificationDetails;
    }

    public Map<String, Object> dataMap() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", id);
        dataMap.put("firstName", firstName);
        dataMap.put("lastName", lastName);
        dataMap.put("username", username);
        dataMap.put("about", about);
        dataMap.put("phoneNo", phoneNo);
        dataMap.put("type", type);
        dataMap.put("userDetails", detail.dataMap());
        return dataMap;
    }
}
