package com.wecanfit.myuser.entity.audit;

import com.wecanfit.myuser.entity.UserEntity;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null || !authentication.isAuthenticated() ){
            return Optional.ofNullable(null);
        }
        //UserEntity entity = (UserEntity)authentication.getPrincipal();
        return Optional.of((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}