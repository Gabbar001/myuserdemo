package com.wecanfit.myuser.entity.follow;

import java.io.Serializable;

public class FollowRecordId implements Serializable {

    private String follower;
    private String following;
}
