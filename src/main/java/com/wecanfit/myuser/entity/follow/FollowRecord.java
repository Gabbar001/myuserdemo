package com.wecanfit.myuser.entity.follow;

import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.entity.audit.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "follow_record") @IdClass(FollowRecordId.class)
public class FollowRecord extends BaseEntity {

    @Id
    @ManyToOne
    @JoinColumn(name = "follower_id", nullable = false)
    private UserEntity follower;

    @Id
    @ManyToOne
    @JoinColumn(name = "following_id", nullable = false)
    private UserEntity following;

    public UserEntity getFollower() {
        return follower;
    }

    public void setFollower(UserEntity follower) {
        this.follower = follower;
    }

    public UserEntity getFollowing() {
        return following;
    }

    public void setFollowing(UserEntity following) {
        this.following = following;
    }
}
