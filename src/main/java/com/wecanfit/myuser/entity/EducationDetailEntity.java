package com.wecanfit.myuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wecanfit.myuser.entity.audit.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "education_detail")
public class EducationDetailEntity extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(name = "school")
    private String school;

    @Column(name = "degree")
    private String degree;

    @Column(name = "field_of_study")
    private String fieldOfStudy;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "activities_socieities")
    private String activitiesAndSocieties;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserEntity user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActivitiesAndSocieties() {
        return activitiesAndSocieties;
    }

    public void setActivitiesAndSocieties(String activitiesAndSocieties) {
        this.activitiesAndSocieties = activitiesAndSocieties;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Map dataMap(){
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("id",id);
        dataMap.put("school",school);
        dataMap.put("degree",degree);
        dataMap.put("fieldOfStudy",fieldOfStudy);
        dataMap.put("startDate",startDate);
        dataMap.put("endDate",endDate);
        dataMap.put("description",description);
        dataMap.put("activitiesAndSocieties",activitiesAndSocieties);
        return dataMap;
    }
}
