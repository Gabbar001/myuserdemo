package com.wecanfit.myuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wecanfit.common.dto.myUser.enums.EmploymentType;
import com.wecanfit.myuser.entity.audit.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "experience_detail")
public class ExperienceDetailEntity extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "is_current_job")
    private Boolean isCurrentJob;

    @Column(name = "title")
    private String title;

    @Column(name = "centre_name")
    private String centreName;

    @Column(name = "location")
    private String location;

    @Column(name = "description" , columnDefinition = "TEXT")
    private String description;

    @Column(name = "employment_type")
    @Enumerated(EnumType.STRING)
    private EmploymentType employmentType;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserEntity user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getCurrentJob() {
        return isCurrentJob;
    }

    public void setCurrentJob(Boolean currentJob) {
        isCurrentJob = currentJob;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Map dataMap(){
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("id",id);
        dataMap.put("startDate",startDate);
        dataMap.put("endDate",endDate);
        dataMap.put("isCurrentJob",isCurrentJob);
        dataMap.put("title",title);
        dataMap.put("centreName",centreName);
        dataMap.put("description",description);
        dataMap.put("employmentType",employmentType);
        return dataMap;
    }
}
