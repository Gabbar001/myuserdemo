package com.wecanfit.myuser.entity;

import com.wecanfit.common.dto.myUser.enums.PhotoType;
import com.wecanfit.common.dto.myUser.enums.PhotoSourceType;
import com.wecanfit.myuser.entity.audit.BaseEntity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "photo")
public class PhotoEntity extends BaseEntity {

    @Id
    private String id;

    @Column(name = "height")
    private Float height;

    @Column(name = "width")
    private Float width;

    @NotNull
    @Column(name = "photo_type")
    @Enumerated(EnumType.STRING)
    private PhotoType photoType;

    @Column(name = "high_res",columnDefinition = "TEXT")
    private String highRes;

    @Column(name = "thumb",columnDefinition = "TEXT")
    private String thumb;

    @NotNull
    @Column(name = "source_type")
    @Enumerated(EnumType.STRING)
    private PhotoSourceType sourceType;

    @Column(name = "is_thumbnail")
    private Boolean isThumbnail;

    @Column(name = "centre_id")
    private String centreId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }

    public String getHighRes() {
        return highRes;
    }

    public void setHighRes(String highRes) {
        this.highRes = highRes;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public PhotoSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(PhotoSourceType sourceType) {
        this.sourceType = sourceType;
    }

    public Boolean getThumbnail() {
        return isThumbnail;
    }

    public void setThumbnail(Boolean thumbnail) {
        isThumbnail = thumbnail;
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId;
    }

    public Map dataMap() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", id);
        dataMap.put("height", height);
        dataMap.put("width", width);
        dataMap.put("photoType", photoType);
        dataMap.put("highRes", highRes);
        dataMap.put("thumb", thumb);
        dataMap.put("sourceType", sourceType);
        dataMap.put("isThumbnail", isThumbnail);
        dataMap.put("centreId", centreId);
        return dataMap;
    }
}
