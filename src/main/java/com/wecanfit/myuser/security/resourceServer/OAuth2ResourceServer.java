package com.wecanfit.myuser.security.resourceServer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter
{


    @Value("${security.oauth2.resource.jwt.key-value}")
    private String jwtSigningKey;

    @Value("${security.oauth2.resource.id}")
    private String RESOURCE_ID;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).tokenServices( createTokenServices() );
//                .tokenStore(tokenStore).stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/api/**").authenticated()
//                .antMatchers("/").permitAll();

        http
                .authorizeRequests().antMatchers("/api/v1/login/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/api/v1/unTracked/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/api/v1/signUp/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/api/v1/notification").permitAll()
                .and()
                .authorizeRequests().antMatchers("/api/v1/notification/users").permitAll()
                .and()
                .authorizeRequests()
                    .antMatchers("/**").permitAll();
//                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }

    @Bean
    public DefaultTokenServices createTokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore( createTokenStore() );
        return defaultTokenServices;
    }

    @Bean
    public TokenStore createTokenStore() {
        return new JwtTokenStore( createJwtAccessTokenConverter() );
    }

    @Bean
    public JwtAccessTokenConverter createJwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setVerifierKey(jwtSigningKey);
        converter.setAccessTokenConverter( new JwtConverter() );
        return converter;
    }
}