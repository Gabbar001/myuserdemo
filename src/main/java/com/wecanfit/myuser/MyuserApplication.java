package com.wecanfit.myuser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

//@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:application.yml")
@SpringBootApplication
@PropertySource("classpath:application.yml")
public class MyuserApplication {

	private static final Logger LOGGER = LogManager.getLogger(MyuserApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MyuserApplication.class, args);
		LOGGER.info("Info level log message");
		LOGGER.debug("Debug level log message");
		LOGGER.error("Error level log message");
	}

}
