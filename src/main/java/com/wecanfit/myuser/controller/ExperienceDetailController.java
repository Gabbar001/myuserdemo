package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.ExperienceDetailRequest;
import com.wecanfit.myuser.service.ExperienceDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class ExperienceDetailController {

    private @Autowired
    ExperienceDetailService service;

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/experience/{id}")
    public ResponseEntity getById(@PathVariable(name = "id") String educationId){
        return ResponseEntity.ok(service.getById(educationId));
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/experience")
    public ResponseEntity getByUserId(){
        return ResponseEntity.ok(service.getByUserId());
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
    @PostMapping(value = "/user/experience")
    public ResponseEntity create(@Valid @RequestBody ExperienceDetailRequest experienceDetailRequest) {
        return ResponseEntity.ok(service.create(experienceDetailRequest));
    }


    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
        @PutMapping(value = "/user/experience/{id}")
    public ResponseEntity update(@PathVariable(name = "id") String experienceId,
            @Valid @RequestBody ExperienceDetailRequest experienceDetailRequest) {
        service.update(experienceId,experienceDetailRequest);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_DELETE')")
    @DeleteMapping("/user/experience/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") String experienceId) {
        service.delete(experienceId);
        return ResponseEntity.ok().build();
    }
}
