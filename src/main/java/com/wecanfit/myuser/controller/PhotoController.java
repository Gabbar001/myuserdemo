package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.PhotoRequest;
import com.wecanfit.common.dto.myUser.response.PhotoResponse;
import com.wecanfit.common.dto.myUser.response.PhotoUploadResponse;
import com.wecanfit.myuser.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class PhotoController {

    private @Autowired
    PhotoService service;

    @PreAuthorize("hasAuthority('PHOTO_WRITE')")
    @PostMapping(value = "/photo/upload")
    public ResponseEntity create(@RequestBody PhotoRequest request) {
        PhotoUploadResponse response = service.create(request);
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('PHOTO_WRITE')")
    @GetMapping(value = "/photo/{photoId}")
    public ResponseEntity getById(@PathVariable(name = "photoId") String photoId){
        PhotoResponse response = service.findById(photoId);
        return ResponseEntity.ok(response);
    }
}
