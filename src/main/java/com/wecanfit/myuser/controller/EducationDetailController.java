package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.EducationDetailRequest;
import com.wecanfit.myuser.service.EducationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class EducationDetailController {

    private @Autowired
    EducationDetailService service;

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/education/{id}")
    public ResponseEntity getById(@PathVariable(name = "id") String educationId){
        return ResponseEntity.ok(service.getById(educationId));
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/education")
    public ResponseEntity getByUserId(){
        return ResponseEntity.ok(service.getByUserId());
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
    @PostMapping(value = "/user/education")
    public ResponseEntity create(@Valid @RequestBody EducationDetailRequest educationDetailRequest) {
        return ResponseEntity.ok(service.create(educationDetailRequest));
    }


    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
    @PutMapping(value = "/user/education/{id}")
    public ResponseEntity update(@PathVariable(name = "id") String educationId,
                                 @Valid @RequestBody EducationDetailRequest educationDetailRequest) {
        service.update(educationId, educationDetailRequest);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_DELETE')")
    @DeleteMapping("/user/education/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") String educationId) {
        service.delete(educationId);
        return ResponseEntity.ok().build();
    }

}
