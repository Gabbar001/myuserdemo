package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.CertificationDetailRequest;
import com.wecanfit.myuser.service.CertificationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class CertificationDetailController {

    private @Autowired
    CertificationDetailService service;

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/certification/{id}")
    public ResponseEntity getById(@PathVariable(name = "id") String educationId){
        return ResponseEntity.ok(service.getById(educationId));
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_READ')")
    @GetMapping("/user/certification")
    public ResponseEntity getByUserId(){
        return ResponseEntity.ok(service.getByUserId());
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
    @PostMapping(value = "/user/certification")
    public ResponseEntity create(@Valid @RequestBody CertificationDetailRequest certificationDetailRequest) {
        return ResponseEntity.ok(service.create(certificationDetailRequest));
    }


    @PreAuthorize("hasAuthority('QUALIFICATION_WRITE')")
    @PutMapping(value = "/user/certification/{id}")
    public ResponseEntity update(@PathVariable(name = "id") String certificationId,
                                 @Valid @RequestBody CertificationDetailRequest certificationDetailRequest) {
        service.update(certificationId,certificationDetailRequest);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('QUALIFICATION_DELETE')")
    @DeleteMapping("/user/certification/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") String certificationId) {
        service.delete(certificationId);
        return ResponseEntity.ok().build();
    }
}
