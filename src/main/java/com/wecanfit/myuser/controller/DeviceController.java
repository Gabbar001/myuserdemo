package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.DeviceRequest;
import com.wecanfit.myuser.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class DeviceController {

    private @Autowired
    DeviceService deviceService;

//    @GetMapping(value = {"/device/token/{userId}","/device/token"})
//    public ResponseEntity getDeviceToken(@PathVariable(name = "userId",required = false) String userId) {
//        return ResponseEntity.ok(deviceService.getDeviceTokenByUserId(userId));
//    }

    @PostMapping("/device/token")
    public ResponseEntity saveDeviceToken(@RequestBody DeviceRequest request) {
        deviceService.saveDeviceToken(request);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/device/token/inactive")
    public ResponseEntity inactiveFcmToken(@RequestParam(name = "deviceId") String deviceId) {
        deviceService.inactiveFcmTokenByDeviceId(deviceId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
