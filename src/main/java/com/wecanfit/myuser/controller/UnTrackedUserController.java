package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.UnTrackedUserRequest;
import com.wecanfit.myuser.service.UnTrackedUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class UnTrackedUserController {

    private @Autowired
    UnTrackedUserService service;

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping(value = "/unTracked/user/{id}")
    public ResponseEntity get(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok(service.get(id));
    }


    @PreAuthorize("hasAuthority('USER_WRITE')")
    @PostMapping(value = "/unTracked/user")
    public ResponseEntity create(@Valid @RequestBody UnTrackedUserRequest request) {
        return ResponseEntity.ok(service.create(request));
    }

}
