package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.enums.IssueType;
import com.wecanfit.common.dto.myUser.enums.SupportStatus;
import com.wecanfit.common.dto.myUser.request.SupportRequest;
import com.wecanfit.myuser.service.SupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class SupportController {

    private @Autowired
    SupportService service;

    @PreAuthorize("hasAuthority('USER_SUPPORT_READ')")
    @GetMapping("/user/support/{id}")
    public ResponseEntity getById(@PathVariable(name = "id") String supportId){
        return ResponseEntity.ok(service.getById(supportId));
    }

    @PreAuthorize("hasAuthority('USER_SUPPORT_READ')")
    @GetMapping("/user/support")
    public ResponseEntity getALL(@RequestParam(name = "p",defaultValue = "0") Integer page,
                                 @RequestParam(name = "l",defaultValue = "10") Integer limit,
                                 @RequestParam(name = "status",defaultValue = "PENDING") SupportStatus status,
                                 @RequestParam(name = "issueType",defaultValue = "OTHER") IssueType issueType){
        return ResponseEntity.ok(service.getAll(status,issueType,page,limit));
    }

    @PreAuthorize("hasAuthority('USER_SUPPORT_WRITE')")
    @PostMapping(value = "/user/support")
    public ResponseEntity create(@Valid @RequestBody SupportRequest supportRequest) {
        service.create(supportRequest);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
