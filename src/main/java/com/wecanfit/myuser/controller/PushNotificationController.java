package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.PushNotificationRequest;
import com.wecanfit.common.dto.myUser.response.PushNotificationResponse;
import com.wecanfit.common.rest.UserRest;
import com.wecanfit.common.service.TokenExtracterService;
import com.wecanfit.common.utils.CommonUtil;
import com.wecanfit.myuser.service.fcm.PushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class PushNotificationController {

    private @Autowired
    PushNotificationService pushNotificationService;

    @PostMapping("/notification")
    public ResponseEntity sendNotificationToUser(@RequestParam(name = "token") String token,
                                                   @RequestBody PushNotificationRequest request){
        if(CommonUtil.validateMicroCommToken(token)) {
            pushNotificationService.sendNotificationToUser(request);
            return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
        }else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/notification/users")
    public ResponseEntity sendNotificationToMultipleUsers( @RequestParam(name = "token") String token,
                                                            @RequestBody PushNotificationRequest request){
        if(CommonUtil.validateMicroCommToken(token)) {
            pushNotificationService.sendNotificationToMultipleUsers(request);
            return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
        }else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @PreAuthorize("hasAuthority('SEND_PUSH_NOTIFICATION')")
    @PostMapping("/notification/topic")
    public ResponseEntity sendTopicNotification(@RequestBody PushNotificationRequest request) {
        pushNotificationService.sendPushNotificationToTopic(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }


    @PostMapping("/notification/token")
    public ResponseEntity sendTokenNotification(@RequestBody PushNotificationRequest request) {
        pushNotificationService.sendPushNotificationToToken(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('SEND_PUSH_NOTIFICATION')")
    @PostMapping("/notification/data")
    public ResponseEntity sendDataNotification(@RequestBody PushNotificationRequest request) {
        pushNotificationService.sendDataNotification(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('SEND_PUSH_NOTIFICATION')")
    @PostMapping("/notification/allToken")
    public ResponseEntity sendNotificationToAllTokens(@RequestBody PushNotificationRequest request) {
        pushNotificationService.sendNotificationToAllTokens(request);
        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }
}
