package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.enums.UserType;
import com.wecanfit.common.dto.myUser.request.UserRequest;
import com.wecanfit.common.dto.myUser.response.UserFullResponse;
import com.wecanfit.common.dto.myUser.response.UserQualificationResponse;
import com.wecanfit.common.dto.myUser.response.UserResponse;
import com.wecanfit.common.enums.Group;
import com.wecanfit.myuser.repository.UserRepository;
import com.wecanfit.myuser.service.FollowService;
import com.wecanfit.myuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class    UserController {

    private @Autowired
    UserService userService;

    private @Autowired
    FollowService followService;

    @Autowired
    UserRepository repository;

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping(value = "/user/{userId}")
    public ResponseEntity getById(@PathVariable(name = "userId") String userId) {
        UserResponse response = userService.getById(userId);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping(value = "/me")
    public ResponseEntity getMe() {
        UserFullResponse  response = userService.getMe();
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping(value = "/user/qualification")
    public ResponseEntity getQualificationDetails() {
        UserQualificationResponse response = userService.getQualification();
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping("/user")
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/username/search")
    public ResponseEntity searchUsername(@RequestParam(name = "username") String username) {
        return ResponseEntity.ok(userService.searchUsername(username));
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping("/user/preLoad")
    public ResponseEntity preLoad() {
        return ResponseEntity.ok(userService.preLoad());
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping("/user/search")
    public ResponseEntity search(@RequestParam(name = "user") String user,
                                 @RequestParam(name = "p", defaultValue = "0") Integer page,
                                 @RequestParam(name = "l", defaultValue = "15") Integer limit) {
        Pageable pageable = PageRequest.of(page, limit);
        return ResponseEntity.ok(userService.searchUser(user, pageable));
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @GetMapping("/user/phone/{phone}")
    public ResponseEntity search(@PathVariable(name = "phone") String phone) {
        return ResponseEntity.ok(userService.getByPhone(phone));
    }

    @PreAuthorize("hasAuthority('USER_WRITE')")
    @PutMapping(value = "/user")
    public ResponseEntity update(@Valid @RequestBody UserRequest userRequest) {
        userService.update(userRequest);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('CENTRE_STAFF_ROLE_WRITE') or hasAuthority('CENTRE_OWNER_ROLE_WRITE')")
    @PutMapping(value = "/user/{userId}/staff")
    public ResponseEntity addCentreStaffGroup(@PathVariable(name = "userId") String userId,
                                              @RequestParam(name = "addGroup",required = false) List<Group> addGroup,
                                              @RequestParam(name = "removeGroup",required = false)  List<Group> removeGroup) {
        userService.CentreStaffGroup(userId,addGroup,removeGroup);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('USER_TYPE_SWITCH')")
    @PutMapping(value = "/user/switch")
    public ResponseEntity changeUserType(@RequestParam(name = "type")UserType type){
        return userService.changeType(type);
    }

    @PreAuthorize("hasAuthority('USER_DELETE')")
    @DeleteMapping("/user")
    public ResponseEntity delete() {
        userService.delete();
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAuthority('USER_FRIENDSHIP_CREATE')")
    @PostMapping(value = "/friendship/create/{userId}")
    public ResponseEntity follow(@PathVariable(name = "userId") String followerId,
                                 @RequestParam(name = "userId") String followingId) {

        followService.follow(followerId, followingId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER_FRIENDSHIP_DESTROY')")
    @PostMapping(value = "/friendship/destroy/{userId}")
    public ResponseEntity unFollow(@PathVariable(name = "userId") String followerId,
                                 @RequestParam(name = "userId") String followingId) {

        followService.unFollow(followerId, followingId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
