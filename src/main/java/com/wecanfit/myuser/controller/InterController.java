package com.wecanfit.myuser.controller;

import com.wecanfit.myuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1")
public class InterController {
    private @Autowired
    UserService userService;

    @GetMapping(value = "/reputation")
    public ResponseEntity getRepuatation(@RequestParam(name = "userId") String userId) {

        Long reputation = userService.getReputation(userId);
        return new ResponseEntity(reputation, HttpStatus.OK);
    }

    @PostMapping(value = "/reputation")
    public ResponseEntity insertReputation(@RequestParam(name = "userId") String userId,
                                           @RequestParam(name = "reputation") Long reputation) {

        userService.updateReputation(userId, reputation);
        return new ResponseEntity(HttpStatus.OK);
    }
}
