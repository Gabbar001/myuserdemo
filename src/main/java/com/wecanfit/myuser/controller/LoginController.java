package com.wecanfit.myuser.controller;

import com.wecanfit.common.dto.myUser.request.UserCreateRequest;
import com.wecanfit.myuser.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@RestController
@RequestMapping("/api/v1")
@Validated
public class LoginController {

    private @Autowired
    LoginService service;


    @GetMapping(value = "/login/requestOtp")
    public ResponseEntity requestOtp(
            @RequestParam(name = "phoneNo") String phoneNo) {
        return ResponseEntity.ok(service.requestOtp(phoneNo));
    }

    @GetMapping(value = "/login/validateOtp")
    public ResponseEntity validateOtp(
            @RequestParam(value = "otp") String otp,
            @RequestParam(value = "phoneNo")String phoneNo) {
        return new ResponseEntity(service.loginWithPhoneNo(otp, phoneNo), HttpStatus.OK);
    }

    @GetMapping(value = "/login/resendOtp")
    public ResponseEntity resendOtp(
            @Valid @Pattern(regexp = "^[6-9]\\d{9}$") @RequestParam(value = "phoneNo", defaultValue = "")String phoneNo) {
        return ResponseEntity.ok(service.requestOtp(phoneNo));
    }


    @PostMapping(value = "signUp/createAccount")
    public ResponseEntity createAccount(@RequestParam(name = "token") String token,
                                 @Valid @RequestBody UserCreateRequest userRequest) {
        return service.createAccount(token,userRequest);
    }

    @PreAuthorize("hasAuthority('CENTRE_REGISTRATION_WRITE')")
    @PostMapping(value = "/centre/{centreId}/user")
    public ResponseEntity createCentreUser(@PathVariable(name = "centreId") String centreId,
                                           @RequestParam(name = "token") String token,
                                        @Valid @RequestBody UserCreateRequest userRequest) {
        return service.createCentreUserAccount(centreId, token,userRequest);
    }
}
