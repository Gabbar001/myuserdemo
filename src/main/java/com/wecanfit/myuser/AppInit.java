package com.wecanfit.myuser;

import com.wecanfit.common.dto.myUser.enums.UserType;
import com.wecanfit.common.enums.Group;
import com.wecanfit.myuser.entity.*;
import com.wecanfit.myuser.repository.GroupRepository;
import com.wecanfit.myuser.repository.UserRepository;
import com.wecanfit.myuser.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AppInit implements ApplicationListener<ContextRefreshedEvent> {

    private @Autowired
    GroupRepository groupRepository;

    private @Autowired
    UserRepository userRepository;

    private @Autowired
    PermissionService permissionService;

    @Value("${wecanfit.update.permissions}")
    Boolean updatePermission;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (updatePermission) {
            permissionService.updatePermission();
        }

        Optional<UserEntity> adminEntity = userRepository.findByUsername("gaurav");
        if (!adminEntity.isPresent()) {
            UserEntity entity = new UserEntity();
            entity.setFirstName("Gaurav");
            entity.setUsername("gaurav");
            entity.setPassword(UUID.randomUUID().toString());
            entity.setType(UserType.ADMIN);
            entity.setPhoneNo("8860083309");
            for (Group group : Group.values()) {
                entity.addGroup(groupRepository.findByName(group.name()));
            }
            userRepository.save(entity);
        }

        Optional<UserEntity> admin2Entity = userRepository.findByUsername("kapil");
        if (!admin2Entity.isPresent()) {
            UserEntity entity = new UserEntity();
            entity.setFirstName("Kapil");
            entity.setUsername("kapil");
            entity.setPassword(UUID.randomUUID().toString());
            entity.setType(UserType.ADMIN);
            entity.setPhoneNo("9911985893");
            for (Group group : Group.values()) {
                entity.addGroup(groupRepository.findByName(group.name()));
            }
            userRepository.save(entity);
        }
    }
}
