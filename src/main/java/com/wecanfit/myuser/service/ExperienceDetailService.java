package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.ExperienceDetailRequest;
import com.wecanfit.common.dto.myUser.response.ExperienceDetailResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.ExperienceDetailEntity;
import com.wecanfit.myuser.mapper.ExperienceDetailMapper;
import com.wecanfit.myuser.repository.ExperienceDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class ExperienceDetailService {

    private @Autowired
    UserService userService;

    private @Autowired
    ExperienceDetailRepository repository;

    @Transactional
    public ExperienceDetailResponse getById(String educationId){
        ExperienceDetailEntity experienceDetailEntity = repository.findById(educationId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return ExperienceDetailMapper.entityToResponse(experienceDetailEntity);
    }

    @Transactional
    public List<ExperienceDetailResponse> getByUserId(){
        List<ExperienceDetailEntity> experienceDetailEntities = repository.findByUserId(userService.getCurrentUserId());
        List<ExperienceDetailResponse> responses = new ArrayList<>();
        if(!Util.isNullOrEmpty(experienceDetailEntities) && experienceDetailEntities.size()>0) {
            for (ExperienceDetailEntity entity : experienceDetailEntities) {
                responses.add(ExperienceDetailMapper.entityToResponse(entity));
            }
        }
        return responses;
    }

    @Transactional
    public ExperienceDetailResponse create(ExperienceDetailRequest experienceDetailRequest){
        ExperienceDetailEntity experienceDetailEntity = ExperienceDetailMapper.requestToEntity(null,experienceDetailRequest);
        experienceDetailEntity.setUser(userService.getCurrentUserRefrence());
        repository.save(experienceDetailEntity);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(experienceDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EXPERIENCE_CREATED, ServiceType.My_USER));
        return ExperienceDetailMapper.entityToResponse(experienceDetailEntity);
    }

    @Transactional
    public ExperienceDetailResponse update(String experienceId,ExperienceDetailRequest experienceDetailRequest){
        ExperienceDetailEntity experienceDetailEntity = repository.findByIdAndUserId(experienceId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(experienceDetailEntity)){
            ExperienceDetailMapper.requestToEntity(experienceDetailEntity,experienceDetailRequest);
            repository.save(experienceDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(experienceDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EXPERIENCE_UPDATED, ServiceType.My_USER));
            return ExperienceDetailMapper.entityToResponse(experienceDetailEntity);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constant.DISSAPROVED);
        }
    }


    @Transactional
    public void delete(String experienceId) {
        ExperienceDetailEntity experienceDetailEntity = repository.findByIdAndUserId(experienceId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(experienceDetailEntity))
        {
            repository.delete(experienceDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(experienceDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EXPERIENCE_DELETED, ServiceType.My_USER));
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constant.DISSAPROVED);
        }
    }

}
