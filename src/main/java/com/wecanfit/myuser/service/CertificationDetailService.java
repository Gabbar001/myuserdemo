package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.CertificationDetailRequest;
import com.wecanfit.common.dto.myUser.response.CertificationDetailResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.CertificationDetailEntity;
import com.wecanfit.myuser.mapper.CertificationDetailMapper;
import com.wecanfit.myuser.repository.CertificationDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class CertificationDetailService {

    private @Autowired
    UserService userService;

    private @Autowired
    CertificationDetailRepository repository;

    @Transactional
    public CertificationDetailResponse getById(String educationId){
        CertificationDetailEntity certificationDetailEntity = repository.findById(educationId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return CertificationDetailMapper.entityToResponse(certificationDetailEntity);
    }

    @Transactional
    public List<CertificationDetailResponse> getByUserId(){
        List<CertificationDetailEntity> certificationDetailEntities = repository.findByUserId(userService.getCurrentUserId());
        List<CertificationDetailResponse> responses = new ArrayList<>();
        if(!Util.isNullOrEmpty(certificationDetailEntities) && certificationDetailEntities.size()>0) {
            for (CertificationDetailEntity entity : certificationDetailEntities) {
                responses.add(CertificationDetailMapper.entityToResponse(entity));
            }
        }
            return responses;
    }

    @Transactional
    public CertificationDetailResponse create(CertificationDetailRequest certificationDetailRequest){
        CertificationDetailEntity certificationDetailEntity = CertificationDetailMapper.requestToEntity(null,certificationDetailRequest);
        certificationDetailEntity.setUser(userService.getCurrentUserRefrence());
        repository.save(certificationDetailEntity);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(certificationDetailEntity.dataMap(),userService.getCurrentUserId(),EventType.CERTIFICATION_CREATED,ServiceType.My_USER));
        return CertificationDetailMapper.entityToResponse(certificationDetailEntity);
    }


    @Transactional
    public CertificationDetailResponse update(String certificationId,CertificationDetailRequest certificationDetailRequest){
        CertificationDetailEntity certificationDetailEntity = repository.findByIdAndUserId(certificationId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(certificationDetailEntity)){
            CertificationDetailMapper.requestToEntity(certificationDetailEntity,certificationDetailRequest);
            repository.save(certificationDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(certificationDetailEntity.dataMap(),userService.getCurrentUserId(),EventType.CERTIFICATION_UPDATED,ServiceType.My_USER));
            return CertificationDetailMapper.entityToResponse(certificationDetailEntity);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constant.DISSAPROVED);
        }
    }


    @Transactional
    public void delete(String certificationId) {
        CertificationDetailEntity certificationDetailEntity = repository.findByIdAndUserId(certificationId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(certificationDetailEntity))
        {
            repository.delete(certificationDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(certificationDetailEntity.dataMap(),userService.getCurrentUserId(),EventType.CERTIFICATION_DELETED,ServiceType.My_USER));
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constant.DISSAPROVED);
        }
    }
}
