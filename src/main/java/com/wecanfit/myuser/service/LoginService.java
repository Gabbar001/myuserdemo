package com.wecanfit.myuser.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.wecanfit.common.dto.myUser.request.UserCreateRequest;
import com.wecanfit.common.dto.myUser.request.UserRequest;
import com.wecanfit.common.dto.myUser.response.LoginResponse;
import com.wecanfit.common.dto.myUser.response.UserResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.enums.Group;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.UserDetailEntity;
import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.entity.UserStats;
import com.wecanfit.myuser.mapper.LoginResponseMapper;
import com.wecanfit.myuser.mapper.UserCreateMapper;
import com.wecanfit.myuser.mapper.UserMapper;
import com.wecanfit.myuser.repository.GroupRepository;
import com.wecanfit.myuser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class LoginService {

    private @Autowired
    UserRepository repository;

    private @Autowired
    GroupRepository groupRepository;

    private @Autowired
    UserService userService;

    private @Autowired
    UserRepository userRepository;

    @Value("${security.jwt.token.clientId}")
    private String clientId;

    @Value("${security.jwt.token.scope}")
    private String[] scope;

    @Value("${security.oauth2.resource.id}")
    private String RESOURCE_ID;

    @Value("${service.sms.enable}")
    private Boolean enableSmsService;

    @Autowired
    SMSNotificationService smsNotificationService;

    @Autowired
    private AuthorizationServerEndpointsConfiguration configuration;

    private static final Integer EXPIRE_MINS = 5;
    private LoadingCache<String,String> otpCache;

    public LoginService() {
        super();
        otpCache = CacheBuilder.newBuilder().
                expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES).build(new CacheLoader<String,String>() {
            public String load(String key) {
                return null;
            }
        });
    }


    @Transactional
    public ResponseEntity requestOtp(String phoneNo) {
            clearOTP(phoneNo);
            String otp = generateOTP(phoneNo);
            System.out.println("########OTP ="+otp);
            if(enableSmsService){
                smsNotificationService.SendMessage(otp, phoneNo);
            }
            return ResponseEntity.ok(phoneNo);
    }

    public ResponseEntity   loginWithPhoneNo(String otp, String phoneNo){
        Boolean validOtp = isValidateOTP(otp, phoneNo);
        if(validOtp){
            Optional<UserEntity> userEntity = userRepository.findByPhoneNo(phoneNo);
            if(userEntity.isPresent()){
                return new ResponseEntity(login(userEntity.get()), HttpStatus.OK);
            }else{
                return new ResponseEntity(generateToken(phoneNo), HttpStatus.OK);
            }
        }else{
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    private Boolean isValidateOTP(String otp, String phoneNo) {
        if (!Util.isNullOrEmpty(otp)) {
            String serverOtp = getValue(phoneNo);
            System.out.println("SERVEROTP: "+serverOtp+ "   OTP: "+otp);
            if (!Util.isNullOrEmpty(serverOtp)) {
                if (otp.equals(serverOtp)) {
                    return true;
                }
            }
        }
        return false;
    }


    @Transactional
    public ResponseEntity createAccount(String token, UserCreateRequest userRequest) {
        if(token.equals(getValue(userRequest.getPhoneNo()))){
            UserEntity userEntity = UserCreateMapper.requestToEntity(userRequest);
            UserEntity savedEntity = createUser(userEntity, userRequest);

            System.out.println(savedEntity.getGroups());
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(userEntity.dataMap(),userEntity.getId(), EventType.USER_ACCOUNT_CREATED, ServiceType.My_USER));
            return new ResponseEntity(login(savedEntity), HttpStatus.OK);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
        }
    }

    @Transactional
    public ResponseEntity createCentreUserAccount(String centreId,  String token, UserCreateRequest userRequest) {
        if(token.equals(getValue(userRequest.getPhoneNo()))){
            UserEntity userEntity = UserCreateMapper.requestToEntityWithManualUsername(userRequest);
            UserEntity savedEntity = createUser(userEntity, userRequest);
            System.out.println(savedEntity.getGroups());
            Map<String, Object> dataMap = userEntity.dataMap();
            dataMap.put("centreId", centreId);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(dataMap,userEntity.getId(), EventType.CENTRE_USER_ACCOUNT_CREATED, ServiceType.My_USER));
            UserResponse response = UserMapper.requestToResponse(savedEntity);
            return new ResponseEntity(response, HttpStatus.CREATED);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
        }
    }

    @Transactional
    public UserEntity createUser(UserEntity userEntity, UserCreateRequest userRequest) {
            UserStats userStats = new UserStats();
            userEntity.setStats(userStats);

            UserDetailEntity userDetailEntity = UserCreateMapper.requestToUserDetailEntity(userRequest.getDetail());
            userEntity.setDetail(userDetailEntity);

            userEntity.addGroup(groupRepository.findByName(Group.USER.name()));
            userEntity.setPassword(UUID.randomUUID().toString());

        return repository.saveAndFlush(userEntity);
    }

    public LoginResponse login(UserEntity entity){
        OAuth2AccessToken oauth2Token = getLoginToken(entity.getId());
        LoginResponse response = LoginResponseMapper.requestToEntity(oauth2Token);
        return response;
    }

    public LoginResponse reLogin(){
        UserEntity entity = repository.findById(userService.getCurrentUserId())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return login(entity);
    }


    public String generateOTP(String phoneNo){
        SecureRandom sr = new SecureRandom();
        int randomNo = 1000 + sr.nextInt(9000);
        String otp = Integer.toString(randomNo);
        otpCache.put(phoneNo,otp);
        return otp;
    }

    public String generateToken(String phoneNo){
        String token = UUID.randomUUID().toString();
        otpCache.put(phoneNo,token);
        return token;
    }


    public String getValue(String key){
        try{
            return otpCache.get(key);
        }catch (Exception e){
            return null;
        }
    }


    public void clearOTP(String phoneNo){
        otpCache.invalidate(phoneNo);
    }

    public OAuth2AccessToken getLoginToken(String username){
        Map<String, String> requestParameters = new HashMap<String, String>();
        Map<String, Serializable> extensionProperties = new HashMap<String, Serializable>();
        boolean approved = true;
        Set<String> responseTypes = new HashSet<String>();

        UserDetails userEntity = userService.loadUserByUsername(username);

        OAuth2Request oauth2Request = new OAuth2Request(
                requestParameters, clientId,
                userEntity.getAuthorities(), approved, new HashSet<>(Arrays.asList(scope)),
                new HashSet<String>(Arrays.asList(RESOURCE_ID)),
                null, responseTypes, extensionProperties);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                userEntity, null, userEntity.getAuthorities());

        OAuth2Authentication auth = new OAuth2Authentication(oauth2Request, authenticationToken);

        AuthorizationServerTokenServices tokenService = configuration.getEndpointsConfigurer().getTokenServices();

        OAuth2AccessToken token = tokenService.createAccessToken(auth);

        return token;
    }
}
