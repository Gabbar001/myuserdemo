package com.wecanfit.myuser.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

@Component
public class SMSNotificationService {

    @Value("${service.sms.apiKey}")
    private String apiKey;

    @Value("${service.sms.templateName}")
    private String templateName;

    @Value("${service.sms.baseUrl}")
    private String baseUrl;

    public void SendMessage(String otp, String phone) {

        StringBuilder sbPostData = new StringBuilder(baseUrl);
        sbPostData.append("/").append(apiKey);
        sbPostData.append("/SMS");
        sbPostData.append("/").append(phone);
        sbPostData.append("/").append(otp);
        sbPostData.append("/").append(templateName);

        System.out.println("URL" + sbPostData.toString());
        try {
            //prepare connection
            URL myURL = new URL(sbPostData.toString());
            URLConnection myURLConnection = myURL.openConnection();
            myURLConnection.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            //reading response
            String response;
            while ((response = reader.readLine()) != null)
                //print response
                System.out.println(response);

            //finally close connection
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}