package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.UnTrackedUserRequest;
import com.wecanfit.common.dto.myUser.response.UnTrackedUserResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.myuser.entity.UnTrackedUserEntity;
import com.wecanfit.myuser.repository.UnTrackedUserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class UnTrackedUserService {

    private @Autowired
    UnTrackedUserRepository repository;

    private @Autowired
    UserService userService;

    @Transactional
    public UnTrackedUserResponse get(String id){
        UnTrackedUserEntity entity = repository.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        UnTrackedUserResponse response  = new ModelMapper().map(entity,UnTrackedUserResponse.class);
        return response;
    }

    @Transactional
    public UnTrackedUserResponse create(UnTrackedUserRequest request){
        UnTrackedUserEntity untrackedUserEntity = new ModelMapper().map(request, UnTrackedUserEntity.class);
        repository.save(untrackedUserEntity);
        UnTrackedUserResponse response  = new ModelMapper().map(untrackedUserEntity,UnTrackedUserResponse.class);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(untrackedUserEntity.dataMap(),userService.getCurrentUserId(), EventType.UNTRACKED_USER_CREATED, ServiceType.My_USER));
        return response;
    }

}
