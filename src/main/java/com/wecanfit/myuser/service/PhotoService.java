package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.PhotoRequest;
import com.wecanfit.common.dto.myUser.response.PhotoResponse;
import com.wecanfit.common.dto.myUser.response.PhotoUploadResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.myuser.entity.PhotoEntity;
import com.wecanfit.myuser.mapper.PhotoMapper;
import com.wecanfit.myuser.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.UUID;


@Component
public class PhotoService {

    private @Autowired
    PhotoRepository repository;

    private @Autowired
    AmazonService amazonService;

    private @Autowired
    UserService userService;

    @Transactional
    public PhotoUploadResponse create(PhotoRequest request){
        PhotoEntity entity = PhotoMapper.requestToEntity(request);
        entity.setId(UUID.randomUUID().toString());
        String putPreSignedURL = amazonService.putPreSignedUrl(entity.getId());
        String getPreSignedURL = amazonService.getObjectUrl(entity.getId());
        entity.setHighRes(getPreSignedURL);
        repository.save(entity);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(entity.dataMap(),userService.getCurrentUserId(), EventType.PHOTO_UPLOADED, ServiceType.My_USER));
        return new PhotoUploadResponse(entity.getId(), putPreSignedURL,getPreSignedURL);
    }

    @Transactional
    public PhotoResponse findById(String photoId) {
        PhotoEntity entity = repository.findById(photoId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));

        PhotoResponse response = PhotoMapper.entityToResponse(entity);
        return response;
    }

    public PhotoEntity load(String photoId){
        PhotoEntity photoEntity = new PhotoEntity();
        photoEntity.setId(photoId);
        return photoEntity;
    }

}
