package com.wecanfit.myuser.service;

import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.entity.UserStats;
import com.wecanfit.myuser.entity.follow.FollowRecord;
import com.wecanfit.myuser.repository.FollowRecordRepository;
import com.wecanfit.myuser.repository.UserRepository;
import com.wecanfit.myuser.repository.UserStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class FollowService {

    private @Autowired
    UserRepository userRepository;

    private @Autowired
    FollowRecordRepository followRecordRepository;

    private @Autowired
    UserStatsRepository userStatsRepository;

    @Transactional
    public void follow(String followerId, String followingId){
        UserEntity followerUser = userRepository.findById(followerId).get();
        UserEntity followToUser = userRepository.findById(followingId).get();

        createFollowRecord(followerUser, followToUser);
        updateFollowerCount(followToUser.getStats(), true);
        updateFollowingCount(followerUser.getStats(), true);
    }

    @Transactional
    public void unFollow(String followerId, String followingId){
        UserEntity followerUser = userRepository.findById(followerId).get();
        UserEntity followToUser = userRepository.findById(followingId).get();

        deleteFollowRecord(followerUser, followToUser);
        updateFollowerCount(followToUser.getStats(), false);
        updateFollowingCount(followerUser.getStats(), false);
    }

    @Transactional
    void createFollowRecord(UserEntity follower, UserEntity following){
        FollowRecord followRecord = new FollowRecord();
        followRecord.setFollower(follower);
        followRecord.setFollowing(following);
        followRecordRepository.save(followRecord);
    }

    @Transactional
    void deleteFollowRecord(UserEntity follower, UserEntity following){
        FollowRecord followRecord = followRecordRepository.findByFollowerAndFollowing(follower, following);
        followRecordRepository.delete(followRecord);
    }

    @Transactional
    void updateFollowingCount(UserStats stats, Boolean isIncrement){
        if(isIncrement){
            stats.setFollowing(stats.getFollowing() + 1);
        }else{
            stats.setFollowing(stats.getFollowing() -1);
        }
        userStatsRepository.save(stats);
    }

    @Transactional
    void updateFollowerCount(UserStats stats, Boolean isIncrement){
        if(isIncrement){
            stats.setFollower(stats.getFollower() + 1);
        }else{
            stats.setFollower(stats.getFollower() -1);
        }
        userStatsRepository.save(stats);
    }
}
