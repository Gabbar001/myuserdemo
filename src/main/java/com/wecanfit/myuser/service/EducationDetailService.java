package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.EducationDetailRequest;
import com.wecanfit.common.dto.myUser.response.EducationDetailResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.EducationDetailEntity;
import com.wecanfit.myuser.mapper.EducationDetailMapper;
import com.wecanfit.myuser.repository.EducationDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class EducationDetailService {

    private @Autowired
    UserService userService;

    private @Autowired
    EducationDetailRepository repository;

    @Transactional
    public EducationDetailResponse getById(String educationId){
        EducationDetailEntity educationDetailEntity = repository.findById(educationId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return EducationDetailMapper.entityToResponse(educationDetailEntity);
    }

    @Transactional
    public List<EducationDetailResponse> getByUserId(){
        List<EducationDetailEntity> educationDetailEntities = repository.findByUserId(userService.getCurrentUserId());
        List<EducationDetailResponse> responses = new ArrayList<>();
        if(!Util.isNullOrEmpty(educationDetailEntities) && educationDetailEntities.size()>0) {
            for (EducationDetailEntity entity : educationDetailEntities) {
                responses.add(EducationDetailMapper.entityToResponse(entity));
            }
        }
            return responses;
    }

    @Transactional
    public EducationDetailResponse create(EducationDetailRequest educationDetailRequest){
        EducationDetailEntity educationDetailEntity = EducationDetailMapper.requestToEntity(null,educationDetailRequest);
        educationDetailEntity.setUser(userService.getCurrentUserRefrence());
        repository.save(educationDetailEntity);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(educationDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EDUCATION_CREATED, ServiceType.My_USER));
        return EducationDetailMapper.entityToResponse(educationDetailEntity);
    }

    @Transactional
    public EducationDetailResponse update(String educationId,EducationDetailRequest educationDetailRequest){
        EducationDetailEntity educationDetailEntity = repository.findByIdAndUserId(educationId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(educationDetailEntity)){
            EducationDetailMapper.requestToEntity(educationDetailEntity,educationDetailRequest);
            repository.save(educationDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(educationDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EDUCATION_UPDATED, ServiceType.My_USER));
            return EducationDetailMapper.entityToResponse(educationDetailEntity);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constant.DISSAPROVED);
        }
    }


    @Transactional
    public void delete(String educationId) {
        EducationDetailEntity educationDetailEntity = repository.findByIdAndUserId(educationId,userService.getCurrentUserId());
        if(!Util.isNullOrEmpty(educationDetailEntity))
        {
            repository.delete(educationDetailEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(educationDetailEntity.dataMap(),userService.getCurrentUserId(), EventType.EDUCATION_DELETED, ServiceType.My_USER));
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constant.DISSAPROVED);
        }
    }
}
