package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.request.DeviceRequest;
import com.wecanfit.common.dto.myUser.response.DeviceResponse;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.DeviceEntity;
import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.repository.DeviceRepository;
import com.wecanfit.myuser.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DeviceService {

    private @Autowired
    UserRepository userRepository;

    private @Autowired
    UserService userService;

    private @Autowired
    DeviceRepository deviceRepository;

    public List<String> getFCMTokensByUserId(String userId){
        return deviceRepository.getFCMTokensByUserIdAndIsActive(userId,true);
    }

    public void saveDeviceToken(DeviceRequest request){
        DeviceEntity entity = deviceRepository.findByDeviceId(request.getDeviceId());
        entity = entity == null ? new DeviceEntity() : entity;

        entity.setFcmToken(request.getFcmToken());
        entity.setDeviceId(request.getDeviceId());
        entity.setUser(userService.getCurrentUserRefrence());
        entity.setActive(true);
        deviceRepository.save(entity);
    }

    public void inactiveFcmTokenByDeviceId(String deviceId){
        DeviceEntity deviceEntity = deviceRepository.findByDeviceId(deviceId);
        if (Util.isNullOrEmpty(deviceEntity)) {
            deviceEntity.setActive(false);
            deviceRepository.save(deviceEntity);
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,Constant.NOT_FOUND);
        }
    }

    public void inactiveFcmToken(String fcmToken){
        DeviceEntity deviceEntity = deviceRepository.findByFcmToken(fcmToken);
        if (Util.isNullOrEmpty(deviceEntity)) {
            deviceEntity.setActive(false);
            deviceRepository.save(deviceEntity);
        }
    }

}
