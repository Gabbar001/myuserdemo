package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myUser.enums.IssueType;
import com.wecanfit.common.dto.myUser.enums.SupportStatus;
import com.wecanfit.common.dto.myUser.request.SupportRequest;
import com.wecanfit.common.dto.myUser.response.SupportResponse;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.myuser.entity.SupportEntity;
import com.wecanfit.myuser.mapper.SupportMapper;
import com.wecanfit.myuser.repository.SupportRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class SupportService {

    private @Autowired
    SupportRepository repository;

    private @Autowired
    UserService userService;

    @Transactional
    public SupportResponse getById(String supportId){
        SupportEntity supportEntity = repository.findById(supportId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return SupportMapper.entityToResponse(supportEntity);
    }

    @Transactional
    public List<SupportResponse> getAll(SupportStatus status, IssueType issueType,Integer page, Integer limit){
        Pageable sortedByCreatedDateDesc =
                PageRequest.of(page, limit, Sort.by("createdDate").descending());
        List<SupportEntity> supportEntities = repository.findAllByStatusAndIssueType(status,issueType,sortedByCreatedDateDesc);
        List<SupportResponse> responses = new ArrayList<>();
        for(SupportEntity entity : supportEntities){
            SupportResponse response = SupportMapper.entityToResponse(entity);
            responses.add(response);
        }
        return responses;
    }

    @Transactional
    public void create(SupportRequest request){
        SupportEntity supportEntity = new ModelMapper().map(request, SupportEntity.class);
        supportEntity.setUser(userService.getCurrentUserRefrence());
        supportEntity.setStatus(SupportStatus.PENDING);
        repository.save(supportEntity);
        AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(supportEntity.dataMap(),userService.getCurrentUserId(), EventType.USER_SUPPORT_REQUEST, ServiceType.My_USER));
    }
}
