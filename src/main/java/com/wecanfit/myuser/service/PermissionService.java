package com.wecanfit.myuser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.GroupEntity;
import com.wecanfit.myuser.entity.PrivilegeEntity;
import com.wecanfit.myuser.entity.RoleEntity;
import com.wecanfit.myuser.repository.GroupRepository;
import com.wecanfit.myuser.repository.PrivilegeRepository;
import com.wecanfit.myuser.repository.RoleRepository;
import com.wecanfit.common.utils.ResourceUtil;
import com.wecanfit.myuser.util.dto.PermissionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Component
public class PermissionService {

    private @Autowired
    GroupRepository groupRepository;

    private @Autowired
    RoleRepository roleRepository;

    private @Autowired
    PrivilegeRepository privilegeRepository;

    @Transactional
    public void updatePermission(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            PermissionDTO permissionDTO = mapper.readValue(new ResourceUtil().loadPermissionJson(), PermissionDTO.class);
            deletePermissions();
            createPermission(permissionDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    public void createPermission(PermissionDTO dto){
        for(Map.Entry<String, List<String>> role : dto.getRoles().entrySet()){
            saveRole(role);
        }

        for(Map.Entry<String, List<String>> group : dto.getGroups().entrySet()){
            saveGroup(group);
        }
    }

    public void saveGroup(Map.Entry<String, List<String>> groupEntry){
        GroupEntity groupEntity = groupRepository.findByName(groupEntry.getKey());
        if(Util.isNullOrEmpty(groupEntity)) {
            groupEntity = new GroupEntity();
            groupEntity.setName(groupEntry.getKey());
        }
        for(String role: groupEntry.getValue()){
            groupEntity.addRole(roleRepository.findByName(role));
        }
        groupRepository.save(groupEntity);
    }


    public void saveRole(Map.Entry<String, List<String>> roleEntry){
        RoleEntity roleEntity = roleRepository.findByName(roleEntry.getKey());

        if(Util.isNullOrEmpty(roleEntity)) {
            roleEntity = new RoleEntity();
            roleEntity.setName(roleEntry.getKey());
        }

        for(String privilege : roleEntry.getValue()){
            PrivilegeEntity privilegeEntity = getPrivilege(privilege);
            roleEntity.addPrivilege(privilegeEntity);
        }
        roleRepository.saveAndFlush(roleEntity);
    }

    public PrivilegeEntity getPrivilege(String privilege){
        PrivilegeEntity privilegeEntity = privilegeRepository.findByName(privilege);
        if(Util.isNullOrEmpty(privilegeEntity)){
            privilegeEntity = new PrivilegeEntity();
            privilegeEntity.setName(privilege);

            privilegeEntity = privilegeRepository.save(privilegeEntity);
        }
        return privilegeEntity;
    }


    public void deletePermissions(){
        List<GroupEntity> groupEntityList = groupRepository.findAll();
        for(GroupEntity groupEntity: groupEntityList){
            groupEntity.getRoles().clear();
            groupRepository.save(groupEntity);
        }

        List<RoleEntity>  roleEntities = roleRepository.findAll();
        for (RoleEntity roleEntity : roleEntities) {
            roleRepository.delete(roleEntity);
        }

        List<PrivilegeEntity>  privilegeEntities = privilegeRepository.findAll();
        for (PrivilegeEntity privilegeEntity : privilegeEntities) {
            privilegeRepository.delete(privilegeEntity);
        }
    }
}
