package com.wecanfit.myuser.service;

import com.wecanfit.common.dto.myPlace.response.ChangeTypeResponse;
import com.wecanfit.common.dto.myUser.enums.UserType;
import com.wecanfit.common.dto.myUser.request.UserRequest;
import com.wecanfit.common.dto.myUser.response.*;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.enums.Group;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.common.rest.CentreRest;
import com.wecanfit.common.utils.Constant;
import com.wecanfit.common.utils.CustomException;
import com.wecanfit.common.utils.Util;
import com.wecanfit.myuser.entity.*;
import com.wecanfit.myuser.mapper.UserDetailMapper;
import com.wecanfit.myuser.mapper.UserMapper;
import com.wecanfit.myuser.repository.GroupRepository;
import com.wecanfit.myuser.repository.PhotoRepository;
import com.wecanfit.myuser.repository.UserRepository;
import com.wecanfit.myuser.repository.UserReputationRepository;
import com.wecanfit.myuser.util.UserUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.*;

@Component
public class UserService implements UserDetailsService {


    private @Autowired
    UserRepository repository;

    private @Autowired
    GroupRepository groupRepository;

    private @Autowired
    PhotoRepository photoRepository;

    private @Autowired
    UserReputationRepository userReputationRepository;

    private @Autowired
    EducationDetailService educationDetailService;

    private @Autowired
    ExperienceDetailService experienceDetailService;

    private @Autowired
    CertificationDetailService certificationDetailService;

    private @Autowired
    LoginService loginService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) {
        Optional<UserEntity> userEntity = repository.findById(s);
        if(!userEntity.isPresent()) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }
        UserEntity user = userEntity.get();
        return new User(
                user.getId(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities());
    }

    @Transactional
    public UserEntity update(UserRequest userRequest) {
        UserEntity userEntity = repository.findById(getCurrentUserId())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,Constant.NOT_FOUND));

        UserMapper.requestToEntity(userRequest, userEntity);
        if(userRequest.getDetail() != null){
            UserDetailEntity detailEntity = userEntity.getDetail() != null ? userEntity.getDetail() : new UserDetailEntity();
            UserDetailMapper.requestToEntity(userRequest.getDetail(), detailEntity);
            userEntity.setDetail(detailEntity);
        }
        if(userRequest.getProfilePhoto() != null && userRequest.getProfilePhoto().getId()!= null){
            userEntity.setProfilePhoto(photoRepository.findById(userRequest.getProfilePhoto().getId()).get());
        }
        if(userRequest.getCoverPhoto() != null && userRequest.getCoverPhoto().getId()!= null) {
            userEntity.setCoverPhoto(photoRepository.findById(userRequest.getCoverPhoto().getId()).get());
        }
        userEntity = repository.save(userEntity);
        return userEntity;
    }

    @Transactional
    public UserResponse getById(String id) {
        UserEntity entity = repository.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        UserResponse response = UserMapper.requestToResponse(entity);
        return response;
    }

    @Transactional
    public UserFullResponse getMe() {
        UserEntity entity = repository.findById(getCurrentUserId())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        return UserMapper.requestToFullResponse(entity);
    }

    @Transactional
    public UserQualificationResponse getQualification() {
        UserQualificationResponse response = new UserQualificationResponse();
        response.setEducationDetails(educationDetailService.getByUserId());
        response.setExperienceDetails(experienceDetailService.getByUserId());
        response.setCertificationDetails(certificationDetailService.getByUserId());
        return response;
    }

    @Transactional
    public UserEntity CentreStaffGroup(String userId,List<Group> addGroup,List<Group> removeGroup) {
        UserEntity userEntity = repository.findById(userId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,Constant.NOT_FOUND));

        if(!Util.isNullOrEmpty(removeGroup) && removeGroup.size()>0){
            for(Group group : removeGroup){
                userEntity.removeGroup(groupRepository.findByName(group.name()));
            }
        }
        if(!Util.isNullOrEmpty(addGroup) && addGroup.size()>0){
            for(Group group : addGroup){
                userEntity.addGroup(groupRepository.findByName(group.name()));
            }
        }
        return userEntity;
    }

    @Transactional
    public ResponseEntity   changeType(UserType type) {
        if(UserType.getUserSwitchTypes().contains(type)){
            UserEntity userEntity = repository.findById(getCurrentUserId())
                    .orElseThrow(() ->
                            new ResponseStatusException(HttpStatus.NOT_FOUND,Constant.NOT_FOUND));

            userEntity.setType(type);
            if(type.equals(UserType.TRAINER)) {
                List<ExperienceDetailResponse> experienceDetailResponses = experienceDetailService.getByUserId();
                List<CertificationDetailResponse> certificationDetailResponses = certificationDetailService.getByUserId();
                if(experienceDetailResponses.size()>0 || certificationDetailResponses.size()>0) {
                    userEntity.addGroup(groupRepository.findByName(Group.TRAINER.name()));
                }
            }else{
                userEntity.removeGroup(groupRepository.findByName(Group.TRAINER.name()));
            }
            repository.save(userEntity);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(userEntity.dataMap(),getCurrentUserId(), EventType.ACCOUNT_TYPE_CHANGE, ServiceType.My_USER));
            return new ResponseEntity(new ChangeTypeResponse(loginService.reLogin()), HttpStatus.OK);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constant.DISSAPROVED);
        }
    }

    @Transactional
    public List<UserEntity> findAll() {
        List<UserEntity> entities = repository.findAllByOrderByCreatedDateDesc();
        return entities;
    }

    @Transactional
    public ResponseEntity searchUsername(String username) {
        Optional<UserEntity> entity = repository.findByUsername(username);
        if(entity.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,Constant.ALREADY_PRESENT);
        }else{
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @Transactional
    public List<UserResponse> searchUser(String username, Pageable pageable) {
        Page<UserEntity> userEntities = repository.findAllByUsernameIsLike("%"+username+"%", pageable);
        return Arrays.asList(new ModelMapper().map(userEntities.getContent(), UserResponse[].class));
    }

    @Transactional
    public UserResponse getByPhone(String phoneNumber) {
        UserEntity entity = repository.findByPhoneNo(phoneNumber)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.NOT_FOUND));
        UserResponse response = UserMapper.requestToResponse(entity);
        return response;
    }

    @Transactional
    public PreLoadResponse preLoad() {
        UserEntity entity = repository.findById(getCurrentUserId())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,Constant.NOT_FOUND));

        PreLoadResponse response = new PreLoadResponse();
        List<String> authorities = new ArrayList<>();
        for(GroupEntity group: entity.getGroups()){
            for(RoleEntity role: group.getRoles()){
                for(PrivilegeEntity privilege: role.getPrivileges()){
                    authorities.add(privilege.getName());
                }
            }
        }
        response.setAuthorities(authorities);
        response.setStaffUser(CentreRest.getCentre());
        response.setUser(getMe());
        response.setCentreUpdates(CentreRest.getCentreUpdate());
        response.setLoginResponse(loginService.reLogin());
        return response;
    }

    @Transactional
    public UserEntity delete() {
        UserEntity userEntity = repository.findById(getCurrentUserId())
                .orElseThrow(() ->
                        new CustomException(HttpStatus.NOT_FOUND.value(),
                                HttpStatus.NOT_FOUND.getReasonPhrase(), Constant.NOT_FOUND));

        userEntity.setFlag(false);
        return repository.save(userEntity);
    }

    @Transactional
    public Long getReputation(String userId) {
        Long response = UserUtil.getReputation(userId);

        if (Util.isNullOrEmpty(response)) {
            UserEntity userEntity = repository.findById(userId).get();
            UserReputationEntity userReputationEntity = userReputationRepository.findByUser(userEntity);
            Long reputation = userReputationEntity.getCount();
            if(response != null){
                UserUtil.insertReputation(userId, reputation);
            }
            return reputation;
        } else {
            return response;
        }
    }

    @Transactional
    public void updateReputation(String userId, Long reputation) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);

        UserReputationEntity userReputationEntity = userReputationRepository.findByUser(userEntity);
        userReputationEntity.setCount(reputation);

        userReputationRepository.save(userReputationEntity);
        UserUtil.insertReputation(userId, reputation);
    }

    public String getCurrentUserId(){
        return (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public UserEntity getCurrentUserRefrence(){
        String id = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        return userEntity;
    }
}
