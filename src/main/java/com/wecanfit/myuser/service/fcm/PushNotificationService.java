package com.wecanfit.myuser.service.fcm;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.wecanfit.common.dto.myUser.request.PushNotificationRequest;
import com.wecanfit.common.dto.wcfUtil.enums.EventType;
import com.wecanfit.common.dto.wcfUtil.enums.ServiceType;
import com.wecanfit.common.mapper.AuditLogMapper;
import com.wecanfit.common.rest.AuditLogRest;
import com.wecanfit.myuser.repository.DeviceRepository;
import com.wecanfit.myuser.service.DeviceService;
import com.wecanfit.myuser.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class PushNotificationService {

    private @Autowired
    FCMService fcmService;

    private @Autowired
    DeviceService deviceService;

    private @Autowired
    UserService userService;

    private @Autowired
    DeviceRepository deviceRepository;

    private static final Logger logger = LogManager.getLogger(PushNotificationService.class);

    public void sendNotificationToUser(PushNotificationRequest request) {
        List<String> fcmTokens = deviceService.getFCMTokensByUserId(request.getUser());
        if(request.getTitle().contains("USER_NAME")){
            request.setTitle(request.getTitle().replace("USER_NAME",userService.getById(request.getUser()).getFullName()));
        }
        for(String fcmToken : fcmTokens) {
            request.setToken(fcmToken);
            try {
                String response = fcmService.sendMessageToToken(request);
                System.out.println("################response: "+response);
                AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(request.dataMap(),"JOB", EventType.SEND_NOTIFICATION, ServiceType.My_USER));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                logger.error(e);
            }
        }
    }

    public void sendNotificationToMultipleUsers( PushNotificationRequest request) {
//        TODO make single query instead of loop          (check this) Done
//        List<String> tokens = deviceRepository.getFCMTokensByIsActiveAndUserIds(true,request.getUsers());
        List<String> tokens = new ArrayList<>();
        for(String userId : request.getUsers()){
            List<String> fcmTokens = deviceService.getFCMTokensByUserId(userId);
            tokens.addAll(fcmTokens);
        }
        request.setFcmTokens(tokens);
        try {
            fcmService.sendNotificationToAllTokens(request);
            AuditLogRest.saveAuditLog(AuditLogMapper.mapAuditLog(request.dataMap(),"JOB", EventType.SEND_NOTIFICATION, ServiceType.My_USER));
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
            logger.error(e);
        }
    }

    public void sendDataNotification(PushNotificationRequest request) {
//        try {
//            fcmService.sendMessageWithData(getSamplePayloadData(), request);
//        } catch (InterruptedException | ExecutionException e) {
//            logger.error(e.getMessage());
//        }
    }

    public void sendPushNotificationToTopic(PushNotificationRequest request) {
        try {
            fcmService.sendMessageToTopic(request);
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e);
        }
    }


    public void sendPushNotificationToToken(PushNotificationRequest request) {
        try {
            fcmService.sendMessageToToken(request);
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e);
        }
    }

    public void sendNotificationToAllTokens(PushNotificationRequest request){
        try {
            fcmService.sendNotificationToAllTokens(request);
        } catch (FirebaseMessagingException e) {
            logger.error(e);
        }
    }
}
