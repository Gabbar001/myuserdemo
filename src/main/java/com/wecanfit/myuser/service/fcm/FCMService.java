package com.wecanfit.myuser.service.fcm;

import com.wecanfit.common.dto.myUser.request.PushNotificationRequest;
import com.wecanfit.myuser.entity.DeviceEntity;
import com.wecanfit.myuser.repository.DeviceRepository;
import com.wecanfit.myuser.service.DeviceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import com.google.firebase.messaging.*;

@Component
public class FCMService {

    private static final Logger logger = LogManager.getLogger(FCMService.class);

    private @Autowired
    DeviceRepository deviceRepository;

    private @Autowired
    DeviceService deviceService;

    public String sendMessageWithData(Map<String, String> data, PushNotificationRequest request)
            throws InterruptedException, ExecutionException {
        Message message = getPreconfiguredMessageWithData(data, request);
        String response = sendAndGetResponse(message);
        logger.info("Sent message with data. Topic: " + request.getTopic() + ", " + response);
        return response;
    }

    public String sendMessageToTopic(PushNotificationRequest request)
            throws InterruptedException, ExecutionException {
        Message message = getPreconfiguredMessageToTopic(request);
        String response = sendAndGetResponse(message);
        logger.info("Sent message without data. Topic: " + request.getTopic() + ", " + response);
        return response;
    }

    public String sendMessageToToken(PushNotificationRequest request)
            throws InterruptedException, ExecutionException {
        Message message = getPreconfiguredMessageToToken(request);
        String response = sendAndGetResponse(message);
        logger.info("Sent message to token. Device token: " + request.getToken() + ", " + response);
        return response;
    }

    public void sendNotificationToAllTokens(PushNotificationRequest request)
            throws FirebaseMessagingException {
        System.out.println(request.getFcmTokens());
        MulticastMessage message = MulticastMessage.builder()
                .setNotification(new Notification(request.getTitle(), request.getMessage()))
//                .putData("title", request.getTitle())
//                .putData("message", request.getMessage())
                .addAllTokens(request.getFcmTokens())
                .build();

        BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
        System.out.println("################response: "+response);
        List<SendResponse> responses = response.getResponses();
        List<String> failedTokens = new ArrayList<>();
        for (int i = 0; i < responses.size(); i++) {
            if (!responses.get(i).isSuccessful()) {
                if (responses.get(i).getException().getErrorCode().equals("404")) {
                    deviceService.inactiveFcmToken(request.getFcmTokens().get(i));
                }
                failedTokens.add(request.getFcmTokens().get(i));
            }
        }
        System.out.println("Success Count: " + response.getSuccessCount());
        System.out.println("Failure Count: " + response.getFailureCount());
        System.out.println("List of tokens that caused failures: " + failedTokens);
    }

    private String sendAndGetResponse(Message message) throws InterruptedException, ExecutionException {
        return FirebaseMessaging.getInstance().sendAsync(message).get();
    }

    private AndroidConfig getAndroidConfig(String topic) {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(2).toMillis()).setCollapseKey(topic)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder().setSound(NotificationParameter.SOUND.getValue())
                        .setColor(NotificationParameter.COLOR.getValue()).setTag(topic).build()).build();
    }

    private ApnsConfig getApnsConfig(String topic) {
        return ApnsConfig.builder()
                .setAps(Aps.builder().setCategory(topic).setThreadId(topic).build()).build();
    }

    private Message getPreconfiguredMessageToToken(PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).setToken(request.getToken())
                .build();
    }

    private Message getPreconfiguredMessageToTopic(PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).setTopic(request.getTopic())
                .build();
    }

    private Message getPreconfiguredMessageWithData(Map<String, String> data, PushNotificationRequest request) {
        return getPreconfiguredMessageBuilder(request).putAllData(data).setTopic(request.getTopic())
                .build();
    }

    private Message.Builder getPreconfiguredMessageBuilder(PushNotificationRequest request) {
        AndroidConfig androidConfig = getAndroidConfig(request.getTopic());
        ApnsConfig apnsConfig = getApnsConfig(request.getTopic());
        return Message.builder()
                .setApnsConfig(apnsConfig).setAndroidConfig(androidConfig).setNotification(
                        new Notification(request.getTitle(), request.getMessage()));
    }
}
