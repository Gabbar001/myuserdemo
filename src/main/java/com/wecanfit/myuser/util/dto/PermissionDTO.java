package com.wecanfit.myuser.util.dto;

import java.util.List;
import java.util.Map;

public class PermissionDTO {

    private Map<String, List<String>> roles;
    private Map<String, List<String>> groups;

    public Map<String, List<String>> getRoles() {
        return roles;
    }

    public void setRoles(Map<String, List<String>> roles) {
        this.roles = roles;
    }

    public Map<String, List<String>> getGroups() {
        return groups;
    }

    public void setGroups(Map<String, List<String>> groups) {
        this.groups = groups;
    }
}
