package com.wecanfit.myuser.util;

import com.wecanfit.common.utils.rest.RequestDetails;
import com.wecanfit.common.utils.rest.RestClient;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class UserUtil {

    static String BASE_URL = "http://localhost:8080/api/v2/query";

    public static void insertReputation(String userId, Long reputation){
        Map<String,Object> param = new HashMap<>();
        param.put("userId",userId);
        param.put("reputation",reputation);
        try {
            new RestClient<String, String>()
                    .execute(new RequestDetails(BASE_URL+"/user/insert/reputation", HttpMethod.PUT,param),
                            "",
                            String.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Long getReputation(String userId){
        Long response =  null;
        try {
            response = new RestClient<String, Long>()
                    .execute(
                            new RequestDetails(BASE_URL+"/user/reputation/"+userId, HttpMethod.GET),
                            "",
                            Long.class);

        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }
}
