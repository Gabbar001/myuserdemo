package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupEntity, String>    {
    GroupEntity findByName(String name);
}
