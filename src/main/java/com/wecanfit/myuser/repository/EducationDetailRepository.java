package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.EducationDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EducationDetailRepository extends JpaRepository<EducationDetailEntity,String> {
    EducationDetailEntity findByIdAndUserId(String id, String userId);

    List<EducationDetailEntity> findByUserId(String userId);
}
