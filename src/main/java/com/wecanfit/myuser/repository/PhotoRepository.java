package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.PhotoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<PhotoEntity, String>    {
}
