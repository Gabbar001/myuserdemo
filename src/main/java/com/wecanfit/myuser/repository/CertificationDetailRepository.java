package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.CertificationDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CertificationDetailRepository extends JpaRepository<CertificationDetailEntity,String> {
    CertificationDetailEntity findByIdAndUserId(String id, String userId);

    List<CertificationDetailEntity> findByUserId(String userId);
}
