package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UserDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailRepository extends JpaRepository<UserDetailEntity, String> {
}
