package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.entity.follow.FollowRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FollowRecordRepository extends JpaRepository<FollowRecord, String> {
    FollowRecord findByFollowerAndFollowing(UserEntity follower, UserEntity following);
}
