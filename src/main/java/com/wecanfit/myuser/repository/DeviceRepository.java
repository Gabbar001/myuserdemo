package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeviceRepository extends JpaRepository<DeviceEntity,String> {

    @Query(value = "select fcm_token from device where user_id = :user_id and is_active = :is_active ;", nativeQuery = true)
    List<String> getFCMTokensByUserIdAndIsActive(@Param("user_id")String userId, @Param("is_active")Boolean isActive);

    @Query(value = "SELECT fcm_token from device where is_active = :is_active and user_id IN :user_id", nativeQuery = true)
    List<String> getFCMTokensByIsActiveAndUserIds(@Param("is_active")Boolean isActive,@Param("user_id")List<String> userIds);

    DeviceEntity findByDeviceId(String deviceId);

    DeviceEntity findByFcmToken(String fcmToken);
}
