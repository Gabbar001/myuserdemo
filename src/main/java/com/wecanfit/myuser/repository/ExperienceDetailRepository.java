package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.ExperienceDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExperienceDetailRepository extends JpaRepository<ExperienceDetailEntity,String> {
    ExperienceDetailEntity findByIdAndUserId(String experienceId,String userId);

    List<ExperienceDetailEntity> findByUserId(String userId);
}
