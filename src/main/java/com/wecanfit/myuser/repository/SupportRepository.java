package com.wecanfit.myuser.repository;

import com.wecanfit.common.dto.myUser.enums.IssueType;
import com.wecanfit.common.dto.myUser.enums.SupportStatus;
import com.wecanfit.myuser.entity.SupportEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SupportRepository extends JpaRepository<SupportEntity,String> {

    List<SupportEntity>  findAllByStatusAndIssueType(SupportStatus status, IssueType issueType, Pageable pageable);
}
