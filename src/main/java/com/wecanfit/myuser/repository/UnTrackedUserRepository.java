package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UnTrackedUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnTrackedUserRepository extends JpaRepository<UnTrackedUserEntity,String> {
}
