package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, String>    {
    List<UserEntity> findAllByOrderByCreatedDateDesc();
    Optional<UserEntity> findByPhoneNo(String phoneNo);
    Optional<UserEntity> findByUsername(String username);

    Page<UserEntity> findAllByUsernameIsLike(String s, Pageable pageable);
}
