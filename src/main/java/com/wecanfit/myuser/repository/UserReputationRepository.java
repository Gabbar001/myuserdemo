package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UserEntity;
import com.wecanfit.myuser.entity.UserReputationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReputationRepository extends JpaRepository<UserReputationEntity, String>{
    UserReputationEntity findByUser(UserEntity userEntity);
}
