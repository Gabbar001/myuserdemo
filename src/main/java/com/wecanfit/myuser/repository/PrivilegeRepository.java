package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.PrivilegeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivilegeRepository extends JpaRepository<PrivilegeEntity, String>    {
    PrivilegeEntity findByName(String name);
}
