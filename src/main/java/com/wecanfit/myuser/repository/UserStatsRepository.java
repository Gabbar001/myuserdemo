package com.wecanfit.myuser.repository;

import com.wecanfit.myuser.entity.UserStats;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserStatsRepository extends JpaRepository<UserStats, String>{
}
